from skills_page import SkillsPage
from category_list_page import CategoryListPage

# Ability to resume

def register_skills(cat_page):
    skills = cat_page.skills
    for skill in skills:
        # print(str(skill))
        print(skill.as_row())

#root = SkillsPage().fetch()
#for [category_name, category_url] in root.categories:
    #print('Category: ' + category_name)
cat_page = CategoryListPage('Lifestyle : Beverages', 'https://www.amazon.com/s?i=alexa-skills&bbn=14284826011&rh=n%3A13727921011%2Cn%3A13727922011%2Cn%3A14284837011%2Cn%3A14284826011%2Cp_n_feature_four_browse-bin%3A15197002011%2Cp_72%3A2661621011&s=review-rank&dc&qid=1576376678&rnid=2661617011&ref=sr_nr_p_72_4', 1)
cat_page.fetch()
#print('\tPage: {0}/{1}'.format(cat_page.page_number, cat_page.total_pages))
register_skills(cat_page)
while cat_page.has_next:
    cat_page = cat_page.next()
    cat_page.fetch()
    #print('\tPage: {0}/{1}'.format(cat_page.page_number, cat_page.total_pages))
    register_skills(cat_page)
print('Done', cat_page.agent, cat_page.page_number)