from urllib.parse import urljoin

class Skill:
    def __init__(self, category, name, url, rating, reviews):
        self.category = category
        self.name = name.strip()
        self.url = url
        self.rating = rating
        self.reviews = reviews

    def __str__(self):
        return "Skill({0}, {1}/{2}, {3})".format(self.name, self.rating, self.reviews, self.url)

    def as_row(self):
        return '\t'.join([
            self.category,
            self.name,
            str(self.rating),
            str(self.reviews),
            self.url
        ])

