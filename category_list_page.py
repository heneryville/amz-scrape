from page import Page
from skill import Skill
from urllib.parse import urljoin

def try_parse_int(content):
    try:
        int(content)
        return True
    except ValueError:
            return False

class CategoryListPage(Page):

    def __init__(self, name, url, page_number):
        super().__init__(url)
        self.name = name
        self.page_number = page_number
    
    def fetch(self):
        super().fetch()
        inner_type_discriminator = self.doc.find(id='search-results')
        if inner_type_discriminator:
            self.inner = CategoryListPage_1(self, self.doc)
        else:
            self.inner = CategoryListPage_2(self, self.doc)
    
    @property
    def skills(self):
        return self.inner.skills

    @property
    def has_next(self):
        return self.inner.has_next

    @property
    def total_pages(self):
        return self.inner.total_pages

    def next(self):
        return self.inner.next()

class CategoryListPage_1:

    def __init__(self, parent, doc):
        self.parent = parent
        self.doc = doc
    
    @property
    def skills(self):
        titleH2s = self.doc.find_all('h2')
        pre_filtered = [ self.parse_from_fragment(h2) for h2 in titleH2s ]
        return [ i for i in pre_filtered if i]

    def parse_from_fragment(self, h2):
        title = h2.get_text()
        link = h2.find_parent('a').get('href')
        link = urljoin(self.parent.url, link)
        root = h2.find_parent(class_='a-section')
        rating_row = root.find(class_="a-col-right").find(class_="a-span-last").find('div')
        if not rating_row.find('a'):
            rating = None
            reviews = None
        else:
            rating_str = rating_row.find(class_="a-icon-star").get_text()
            rating = float(rating_str.split(' ')[0])
            reviews = rating_row.find('a', class_="a-text-normal").get_text()
            reviews = int(reviews.replace(',',''))
        return Skill(self.parent.name, title, link, rating, reviews)


    @property
    def has_next(self):
        return  not not self.doc.find(id="pagnNextLink")

    @property
    def total_pages(self):
        page_navs = self.doc.find(id='pagn')
        if page_navs:
            page_buttons = [button.get_text() for button in page_navs.find_all('span')]
        else:
            page_navs = self.doc.find(class_='a-pagination')
            page_buttons = [button.get_text() for button in page_navs.find_all('li')]
        parsables_buttons = [ button for button in page_buttons if try_parse_int(button)  ]
        return int(parsables_buttons[-1])

    def next(self):
        next = self.doc.find(id="pagnNextLink")
        if not next: return None
        url = self.parent.as_url(next.get('href'))
        return CategoryListPage(self.parent.name, url, self.parent.page_number + 1)

class CategoryListPage_2:

    def __init__(self, parent, doc):
        self.parent = parent
        self.doc = doc
    
    @property
    def skills(self):
        titleH2s = self.doc.find_all('h2')
        pre_filtered = [ self.parse_from_fragment(fragment) for fragment in titleH2s ]
        return [ i for i in pre_filtered if i]

    def parse_from_fragment(self, h2):
        title = h2.get_text()
        link = h2.find('a')
        if not link:
            return None
        link = link.get('href')
        link = urljoin(self.parent.url, link)
        possible_rating_rows = h2.parent.parent.find_all('div',recursive=False)
        if len(possible_rating_rows) > 1 and possible_rating_rows[1].find(attrs={'data-action':'a-popover'}):
            rating_row = possible_rating_rows[1]
            rating_str = rating_row.find(class_="a-icon-star-small").get_text()
            rating = float(rating_str.split(' ')[0])
            reviews = rating_row.find('span', class_="a-size-base").get_text()
            reviews = int(reviews.replace(',',''))
        else:
            rating = None
            reviews = None
        return Skill(self.parent.name, title, link, rating, reviews)

    @property
    def has_next(self):
        last = self.doc.find(class_="a-last")
        if not last:
            return False
        a = last.find('a')
        return not not a

    @property
    def total_pages(self):
        page_navs = self.doc.find(class_='a-pagination')
        page_buttons = [button.get_text() for button in page_navs.find_all('li')]
        parsables_buttons = [ button for button in page_buttons if try_parse_int(button)  ]
        return int(parsables_buttons[-1])

    def next(self):
        last = self.doc.find(class_="a-last")
        a = last.find('a')
        url = self.parent.as_url(a.get('href'))
        return CategoryListPage(self.parent.name, url, self.parent.page_number + 1)
