from page import Page

class SkillsPage(Page):

    def __init__(self):
        super().__init__('https://www.amazon.com/s/?url=search-alias%3Dalexa-skills')

    @property
    def categories(self):
        left_nav = self.doc.find('div',id="leftNav")
        alexa_skills = left_nav.find(text="Alexa Skills")
        links = alexa_skills.find_parent('ul').find_all('a')
        return [ [a.get_text(), self.as_url(a.get('href'))] for a in links ]