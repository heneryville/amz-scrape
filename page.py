import time
import requests
from urllib.parse import urljoin
from bs4 import BeautifulSoup 
from user_agent import an_agent

class Page:
    def __init__(self, url):
        self.url = url
    
    def fetch(self):
        time.sleep(5.5)
        self.agent = an_agent()
        response = requests.get(self.url
        ,headers={'User-Agent': self.agent}
        )
        content = response.text
        """
        with open('page.html','w+') as f:
            f.write(content)
        """
        # content = open('sample.html').read()
        self.doc = BeautifulSoup(content,'html5lib')
        return self
    
    def as_url(self, relative):
        return urljoin(self.url, relative)

